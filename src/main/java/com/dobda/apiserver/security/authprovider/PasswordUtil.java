package com.dobda.apiserver.security.authprovider;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

public class PasswordUtil {
    public static byte[] genDigest(String plain, ZonedDateTime regDate) throws NoSuchAlgorithmException {
        String salt = regDate.withZoneSameInstant(ZoneId.of("UTC")).format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS'Z'"));

        return genDigest(plain, salt);
    }

    public static byte[] genDigest(String plain, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        String saltedText = String.format("%0128x", new BigInteger(1, md.digest(salt.getBytes(StandardCharsets.UTF_8)))).substring(0, 64) + plain;
        byte[] digest = md.digest(saltedText.getBytes(StandardCharsets.UTF_8));

        return digest;
    }

    public static boolean compare(byte[] digest1, byte[] digest2) {
        Base64.Encoder encoder = Base64.getEncoder();
        String d1 = encoder.encodeToString(digest1);
        String d2 = encoder.encodeToString(digest2);

        return d1.equals(d2);
    }
}
