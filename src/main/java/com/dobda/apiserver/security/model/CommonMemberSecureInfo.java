package com.dobda.apiserver.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.Base64;


@Getter @Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class CommonMemberSecureInfo implements UserDetails{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "INT UNSIGNED")
    private Long id;
	
    @NotNull @Size(max = 255)
    @Column(unique = true)
    private String email;

    @NotNull @Size(max = 64)
    @Column(columnDefinition = "VARBINARY(64)")
    private byte[] pwd;

    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;
    
    public CommonMemberSecureInfo(String email, byte[] pwd) {
        this.email = email;
        this.pwd = pwd;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return Base64.getEncoder().encodeToString(this.pwd); // 길이 86~88
    }

	@Override
	@JsonIgnore
	public String getUsername() {
		return getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isAccountNonExpired() &&
                isAccountNonLocked() &&
                isCredentialsNonExpired();
	}

}
