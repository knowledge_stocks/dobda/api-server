package com.dobda.apiserver.api.sms;

import com.dobda.apiserver.api.sms.entity.SmsAuthKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;

public interface SmsAuthKeyRepository extends JpaRepository<SmsAuthKey, String> {
    public void deleteByExpireTimeBefore(ZonedDateTime time);
}
