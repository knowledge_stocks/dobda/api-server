package com.dobda.apiserver.api.payment.dto;

import com.dobda.apiserver.common.util.openbank.account.AccountResponse;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class MyAccountsInfo {
    List<Account> accounts;

    @Builder
    @Data
    public static class Account {
        private String fintechNum;
        private String bankName;
        private String accountNumMasked;
        private String accountHolderName;

        private static Account of(AccountResponse.Account account) {
            if(account == null) {
                return null;
            }
            return Account.builder()
                    .fintechNum(account.getFintech_use_num())
                    .bankName(account.getBank_name())
                    .accountNumMasked(account.getAccount_num_masked())
                    .accountHolderName(account.getAccount_holder_name())
                    .build();
        }
    }

    public static MyAccountsInfo of(AccountResponse response) {
        if(response == null || !response.getRsp_code().equals("A0000")) {
            return null;
        }
        return MyAccountsInfo.builder()
                .accounts(response.getRes_list().stream()
                        .map(Account::of).collect(Collectors.toList()))
                .build();
    }
}
