package com.dobda.apiserver.api.order;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.dto.OrderInfoDTO;
import com.dobda.apiserver.api.order.dto.RetryRefundAllDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class OrderController {
	
	@Autowired
	private OrderService orderService;

    @Autowired
    private OrderNonTranService orderNonTranService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
	
	// 주문 정보 조회 //
	@GetMapping("/user/order/{id}")
	public ResponseEntity infoOrder(@PathVariable("id") BigInteger id) {
		try {
			OrderInfoDTO result = orderService.findOrder(id);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	// 헬퍼 주문 수락 //
	@PutMapping("/helper/order/{id}/accept")
	public ResponseEntity acceptOrder(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
		try {
			NotificationMember noti = orderService.orderAccept((Long) authentication.getPrincipal(), id);
            if(noti != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
    // 의뢰 취소 //
    @PutMapping("/user/order/{id}/cancel")
    public ResponseEntity calcelOrder(
            Authentication authentication,
            @PathVariable("id") BigInteger id)
    {
        try {
            List<NotificationMember> notiList = orderService.orderCancel((Long) authentication.getPrincipal(), id);
            notiList.forEach(noti -> {
                if(noti != null) {
                    simpMessagingTemplate.convertAndSend(
                            "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
                }
            });
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

	// 의뢰 완료 //
	@PutMapping("/user/order/{id}/complete")
	public ResponseEntity completeOrder(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
		try {
			NotificationMember noti = orderService.orderComplete((Long) authentication.getPrincipal(), id);
            if(noti != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
    @GetMapping("/user/order/list")
    public ResponseEntity myRequestOrderList(
            Authentication authentication,
            @RequestParam(value="page", required = false) Integer page,
            @RequestParam(value="states", required = false) int[] states
    ) {
        try {
            Page<OrderInfoDTO> result = orderService.myRequestOrderList(
                    page, (Long)authentication.getPrincipal(), states);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/helper/order/list")
    public ResponseEntity myHelperOrderList(
            Authentication authentication,
            @RequestParam(value="page", required = false) Integer page,
            @RequestParam(value="states", required = false) int[] states
    ) {
        try {
            Page<OrderInfoDTO> result = orderService.myHelperOrderList(
                    page, (Long)authentication.getPrincipal(), states);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/user/order/{id}/refund")
    public ResponseEntity retryRefund(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
        try {
            orderService.retryRefund((Long)authentication.getPrincipal(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/user/order/refund")
    public ResponseEntity retryRefundAll(
            Authentication authentication,
            @ModelAttribute @Valid RetryRefundAllDto dto
    ) {
        try {
            orderNonTranService.retryRefundAll((Long)authentication.getPrincipal(), dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/helper/order/{id}/calc")
    public ResponseEntity retryCalc(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
        try {
            orderService.retryCalc((Long)authentication.getPrincipal(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/helper/order/calc")
    public ResponseEntity retryCalcAll(
            Authentication authentication,
            @ModelAttribute @Valid RetryRefundAllDto dto
    ) {
        try {
            orderNonTranService.retryCalcAll((Long)authentication.getPrincipal(), dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
