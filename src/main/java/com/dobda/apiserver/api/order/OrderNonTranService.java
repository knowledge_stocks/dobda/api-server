package com.dobda.apiserver.api.order;

import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.helper.MemberHelperRepository;
import com.dobda.apiserver.api.member.helper.MemberHelperService;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.order.dto.RetryRefundAllDto;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.common.enums.OrderState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderNonTranService {

    @Autowired private OrderService orderService;
    @Autowired private OrderRepository orderRepository;
    @Autowired private MemberUserRepository memberUserRepository;
    @Autowired private MemberHelperRepository memberHelperRepository;
    @Autowired private MemberHelperService memberHelperService;

    public void retryRefundAll(Long memberId, RetryRefundAllDto dto) {
        Optional<MemberUser> memberOption = memberUserRepository.findById(memberId);
        if(memberOption.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser member = memberOption.get();
        // 보안 위험, 입력받은 계좌 정보가 사용자의 계좌가 맞는지 확인할 필요가 있음

        List<Order> orderList = orderRepository.findAllByMemberUser_IdAndState(memberId, OrderState.WAITING_REFUND);
        if(orderList == null || orderList.size() == 0) {
            return;
        }

        String userCode = member.getBankUserCode();
        String fintech = dto.getFintech();
        String bankName = dto.getBankName();

        orderList.forEach(order -> {
            try {
                orderService.updateBankInfo(order, dto);
                orderService.retryRefund(order, fintech, bankName, userCode);
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        });
    }

    public void retryCalcAll(Long memberId, RetryRefundAllDto dto) {
        Optional<MemberHelper> memberOption = memberHelperRepository.findById(memberId);
        if(memberOption.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = memberOption.get();
        // 보안 위험, 입력받은 계좌 정보가 사용자의 계좌가 맞는지 확인할 필요가 있음

        List<Order> orderList = orderRepository.findAllByMemberHelper_IdAndState(memberId, OrderState.WAITING_CALC);
        if(orderList == null || orderList.size() == 0) {
            return;
        }

        memberHelperService.updateBankInfo(helper, dto);

        String userCode = helper.getMemberUser().getBankUserCode();
        String fintech = helper.getFintechNum();
        String bankName = helper.getBankName();

        orderList.forEach(order -> {
            try {
                orderService.retryCalc(order, fintech, bankName, userCode);
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        });
    }
}
