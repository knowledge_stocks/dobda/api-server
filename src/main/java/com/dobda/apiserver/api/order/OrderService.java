package com.dobda.apiserver.api.order;

import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.ForbiddenException;
import com.dobda.apiserver.api.common.errorhandling.InternalServerErrorException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.NotificationMemberRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.dto.OrderInfoDTO;
import com.dobda.apiserver.api.order.dto.RetryRefundAllDto;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.api.requestboard.repository.RequestRepository;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.PaymentMethod;
import com.dobda.apiserver.common.enums.RequestState;
import com.dobda.apiserver.common.enums.ResourceType;
import com.dobda.apiserver.common.util.openbank.OpenBankUtils;
import com.dobda.apiserver.common.util.openbank.payment.DepositResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderService {
	
	private final OrderRepository orderRepository;
	private final RequestRepository requestRepository;
    private final NotificationMemberRepository notificationMemberRepository;

	// 주문 정보 조회 //
	public OrderInfoDTO findOrder(BigInteger id) {
		Optional<Order> order = orderRepository.findById(id);
		if(order.isEmpty()) {
			throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
		}
		return OrderInfoDTO.fromEntity(order.get());
	}
	
	// 헬퍼 의뢰 승락 //
	@Transactional
	public NotificationMember orderAccept(Long helperId, BigInteger id) {
		Optional<Order> findOrder = orderRepository.findById(id);
		if(findOrder.isEmpty()) {
			throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
		}
		Order order = findOrder.get();
        MemberHelper helper = order.getMemberHelper();
        if(helper == null || !helper.getId().equals(helperId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
		if(order.getState() != OrderState.WAITING) {
			throw new BadRequestException("이미 진행되거나 취소된 주문입니다.");
		}
        MemberUser member = order.getMemberUser();
        if(member == null) {
            throw new BadRequestException("상대 사용자가 탈퇴한 주문은 수락할 수 없습니다.");
        }
		order.setState(OrderState.ACCEPTED);
        helper.setTotalHelpCnt(helper.getTotalHelpCnt() + 1);

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(member)
                .message("'" + order.getRequest().getTitle() + "' 의뢰의 주문이 수락되었습니다.")
                .targetType(ResourceType.ORDER)
                .targetId(order.getId())
                .build();
        notificationMemberRepository.save(newNoti);

        return newNoti;
	}
	
	// 의뢰 취소 요청 //
	@Transactional
	public List<NotificationMember> orderCancel(Long memberId, BigInteger id) {
		Optional<Order> findOrder = orderRepository.findById(id);
		if(findOrder.isEmpty()) {
			throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
		}
		Order order = findOrder.get();

        MemberUser member = order.getMemberUser();
        MemberHelper helper = order.getMemberHelper();
		if((member == null || !member.getId().equals(memberId)) &&
            (helper == null || !helper.getId().equals(memberId))) {
			throw new ForbiddenException("권한이 없습니다.");
		}
        if(order.getState()==OrderState.ACCEPTED) {
            throw new BadRequestException("수락된 주문은 취소할 수 없습니다.");
        }
        if(order.getState()==OrderState.COMPLETE || order.getState()==OrderState.WAITING_CALC) {
            throw new BadRequestException("이미 완료된 주문입니다.");
        }
        if(order.getState()==OrderState.CANCELED || order.getState()==OrderState.WAITING_REFUND) {
            throw new BadRequestException("이미 취소된 주문입니다.");
        }

        // 취소 작업 시작
        Request request = order.getRequest();
        if(request.getState() != RequestState.DELETED &&
            request.getState() != RequestState.BLOCK &&
            request.getState() != RequestState.COMPLETE
        ) {
            request.setState(RequestState.POSTED);
        }

        List<NotificationMember> notiList = new ArrayList<>();

        MemberUser notiMember = null;
        if(member != null && member.getId().equals(memberId)) {
            if(helper != null) {
                notiMember = helper.getMemberUser();
            }
        } else if(helper != null && helper.getId().equals(memberId)) {
            notiMember = member;
        }
        if(notiMember != null) {
            NotificationMember newNoti = NotificationMember.builder()
                    .memberUser(member)
                    .message("'" + request.getTitle() + "' 의뢰의 주문이 취소되었습니다.")
                    .targetType(ResourceType.ORDER)
                    .targetId(order.getId())
                    .build();
            notificationMemberRepository.save(newNoti);

            notiList.add(newNoti);
        }

        boolean isRefund = true;
        if (order.getPayMethod() == PaymentMethod.ACCOUNT_TRANSFER) {
            DepositResponse response = OpenBankUtils.deposit(
                            OpenBankUtils.getOrgToken().getAccessToken(),
                            order.getRequesterFintech(),
                            order.getRequesterBankName(),
                            member == null ? order.getId().toString() : member.getBankUserCode(),
                            order.getAmount(), true);
            if (!response.getRsp_code().equals("A0000")) {
                isRefund = false;
            }
        }

        if(isRefund) {
            order.setState(OrderState.CANCELED);
        } else {
            order.setState(OrderState.WAITING_REFUND);

            if(member != null) {
                NotificationMember newNoti = NotificationMember.builder()
                        .memberUser(member)
                        .message("입금 실패한 환불금액이 있습니다. 계좌 인증을 다시해주세요.")
                        .targetType(ResourceType.ORDER)
                        .targetId(order.getId())
                        .build();
                notificationMemberRepository.save(newNoti);

                notiList.add(newNoti);
            }
        }

        return notiList;
	}

	// 의뢰 완료 및 헬퍼에게 입금 //
	@Transactional
	public NotificationMember orderComplete(Long requesterId, BigInteger id) {
		Optional<Order> findOrder = orderRepository.findById(id);
		if(findOrder.isEmpty()) {
			throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
		}
		Order order = findOrder.get();
        MemberUser member = order.getMemberUser();
        if(member == null || !member.getId().equals(requesterId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        if(order.getState()==OrderState.WAITING) {
            throw new BadRequestException("헬퍼가 수락하지 않은 주문입니다.");
        }
        if(order.getState()==OrderState.COMPLETE || order.getState()==OrderState.WAITING_CALC) {
            throw new BadRequestException("이미 완료된 주문입니다.");
        }
        if(order.getState()==OrderState.CANCELED || order.getState()==OrderState.WAITING_REFUND) {
            throw new BadRequestException("이미 취소된 주문입니다.");
        }

        // 완료 작업 시작
        Request request = order.getRequest();
        if(request.getState() != RequestState.DELETED &&
            request.getState() != RequestState.BLOCK) {
            request.setState(RequestState.COMPLETE);
        }
        member.setTotalRequestCount(member.getTotalRequestCount() + 1);
        MemberHelper helper = order.getMemberHelper();
        if(helper != null) {
            helper.setTotalCompleteCnt(helper.getTotalCompleteCnt() + 1);
        }

		boolean isCalc = true;
        if (order.getPayMethod() == PaymentMethod.ACCOUNT_TRANSFER) {
            if(helper != null)
            {
                DepositResponse response = OpenBankUtils.deposit(
                        OpenBankUtils.getOrgToken().getAccessToken(),
                        helper.getFintechNum(),
                        helper.getBankName(),
                        helper.getMemberUser().getBankUserCode(),
                        order.getAmount(), false);
                if(!response.getRsp_code().equals("A0000")) {
                    isCalc = false;
                }
            } else {
                isCalc = false;
            }
        }

        if(isCalc) {
            order.setState(OrderState.COMPLETE);

            if(helper != null) {
                NotificationMember newNoti = NotificationMember.builder()
                        .memberUser(helper.getMemberUser())
                        .message("'" + request.getTitle() + "' 의뢰의 주문이 완료되었습니다.")
                        .targetType(ResourceType.ORDER)
                        .targetId(order.getId())
                        .build();
                notificationMemberRepository.save(newNoti);

                return newNoti;
            }
        } else {
            order.setState(OrderState.WAITING_CALC);

            if(helper != null) {
                NotificationMember newNoti = NotificationMember.builder()
                        .memberUser(helper.getMemberUser())
                        .message("정산 실패한 대금이 있습니다. 계좌 인증을 다시해주세요.")
                        .targetType(ResourceType.ORDER)
                        .targetId(order.getId())
                        .build();
                notificationMemberRepository.save(newNoti);

                return newNoti;
            }
        }

        return null;
	}

    public Page<OrderInfoDTO> myRequestOrderList(Integer page, Long memberId, int[] states){
        EnumSet<OrderState> stateSet = EnumSet.noneOf(OrderState.class);
        if(states != null) {
            for (int code: states) {
                OrderState state = OrderState.fromCode(code);
                if(state != null) {
                    stateSet.add(state);
                }
            }
        }
        if(stateSet.size() == 0) {
            stateSet = EnumSet.allOf(OrderState.class);
        }

        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "id");
        Page<Order> findResults = orderRepository.findByMemberUser_IdAndStateIn(memberId, stateSet, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(OrderInfoDTO::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    public Page<OrderInfoDTO> myHelperOrderList(Integer page, Long helperId, int[] states){
        EnumSet<OrderState> stateSet = EnumSet.noneOf(OrderState.class);
        if(states != null) {
            for (int code: states) {
                OrderState state = OrderState.fromCode(code);
                if(state != null) {
                    stateSet.add(state);
                }
            }
        }
        if(stateSet.size() == 0) {
            stateSet = EnumSet.allOf(OrderState.class);
        }

        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "id");
        Page<Order> findResults = orderRepository.findByMemberHelper_IdAndStateIn(helperId, stateSet, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(OrderInfoDTO::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    @Transactional
    public void retryRefund(Order order, String fintech, String bankName, String userCode) {
        order.setState(OrderState.CANCELED);

        DepositResponse response = OpenBankUtils.deposit(
                OpenBankUtils.getOrgToken().getAccessToken(),
                fintech,
                bankName,
                userCode,
                order.getAmount(), true);
        if (!response.getRsp_code().equals("A0000")) {
            throw new InternalServerErrorException("환불에 실패했습니다. 에러메시지: " + response.getRsp_message());
        }
    }

    @Transactional
    public void retryCalc(Order order, String fintech, String bankName, String userCode) {
        order.setState(OrderState.COMPLETE);

        DepositResponse response = OpenBankUtils.deposit(
                OpenBankUtils.getOrgToken().getAccessToken(),
                fintech,
                bankName,
                userCode,
                order.getAmount(), false);
        if (!response.getRsp_code().equals("A0000")) {
            throw new InternalServerErrorException("정산에 실패했습니다. 에러메시지: " + response.getRsp_message());
        }
    }

    @Transactional
    public void retryCalc(Long helperId, BigInteger id) {
        Optional<Order> findOrder = orderRepository.findById(id);
        if(findOrder.isEmpty()) {
            throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
        }
        Order order = findOrder.get();

        MemberHelper helper = order.getMemberHelper();
        if(helper == null || !helper.getId().equals(helperId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        if(order.getState() != OrderState.WAITING_CALC) {
            throw new BadRequestException("주문 상태가 올바르지 않습니다.");
        }

        order.setState(OrderState.COMPLETE);

        DepositResponse response = OpenBankUtils.deposit(
                OpenBankUtils.getOrgToken().getAccessToken(),
                helper.getFintechNum(),
                helper.getBankName(),
                helper.getMemberUser().getBankUserCode(),
                order.getAmount(), false);
        if (!response.getRsp_code().equals("A0000")) {
            throw new InternalServerErrorException("정산에 실패했습니다. 에러메시지: " + response.getRsp_message());
        }
    }

    @Transactional
    public void retryRefund(Long memberId, BigInteger id) {
        Optional<Order> findOrder = orderRepository.findById(id);
        if(findOrder.isEmpty()) {
            throw new NotFoundException("주문 정보를 찾을 수 없습니다.");
        }
        Order order = findOrder.get();

        MemberUser member = order.getMemberUser();
        if(member == null || !member.getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }
        if(order.getState() != OrderState.WAITING_REFUND) {
            throw new BadRequestException("주문 상태가 올바르지 않습니다.");
        }

        order.setState(OrderState.CANCELED);

        DepositResponse response = OpenBankUtils.deposit(
                OpenBankUtils.getOrgToken().getAccessToken(),
                order.getRequesterFintech(),
                order.getRequesterBankName(),
                member.getBankUserCode(),
                order.getAmount(), true);
        if (!response.getRsp_code().equals("A0000")) {
            throw new InternalServerErrorException("환불에 실패했습니다. 에러메시지: " + response.getRsp_message());
        }
    }

    @Transactional
    public void updateBankInfo(Order order, RetryRefundAllDto dto) {
        order.setRequesterBankName(dto.getBankName());
        order.setRequesterFintech(dto.getFintech());
    }
}
