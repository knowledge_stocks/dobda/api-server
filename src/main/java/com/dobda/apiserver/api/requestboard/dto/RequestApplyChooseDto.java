package com.dobda.apiserver.api.requestboard.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestApplyChooseDto {

    @NotNull private Integer payMethod;
	private String bankName;
	private String fintech;
}
