package com.dobda.apiserver.api.requestboard.controller;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.requestboard.dto.RequestPostAddDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostInfoDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostListInfoDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostModDto;
import com.dobda.apiserver.api.requestboard.service.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;

@RestController
@RequestMapping("/api")
public class RequestController {
	
	private final Logger logger = LoggerFactory.getLogger(RequestService.class);
	
	@Autowired
	private RequestService requestService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
	
	// 글 등록 
	@PostMapping("/user/request")
	public ResponseEntity addRequestPost(Authentication authentication, @ModelAttribute @Valid RequestPostAddDto addDto) {
		logger.info("Passing RequestController -> addRequestPost");
        logger.info("Controller Member Id: " + authentication.getPrincipal());

        try {
            BigInteger requestId = requestService.addRequest((Long)authentication.getPrincipal(), addDto);
            return new ResponseEntity(requestId, HttpStatus.CREATED);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	/*
	 * 글 검색 By 제목 
	 * */
	@GetMapping("/public/request/search")
	public ResponseEntity searchRequestPost(
            @RequestParam(value="page", required = false) Integer page,
            @RequestParam(value="sort", required = false) String sort,
            @RequestParam(value="keyword", required = false) String keyword,
            @RequestParam(value="onlyWaiting", required = false) Boolean onlyWaiting,
            @RequestParam(value="categories", required = false) int[] categories,
            @RequestParam(value="minPrice", required = false) Long minPrice,
            @RequestParam(value="maxPrice", required = false) Long maxPrice,
            @RequestParam(value="startX") Double startX,
            @RequestParam(value="startY") Double startY,
            @RequestParam(value="endX") Double endX,
            @RequestParam(value="endY") Double endY
    ) {
        try {
            Page<RequestPostListInfoDto> result = requestService.searchRequest(
                    page, sort, keyword, onlyWaiting, categories, minPrice, maxPrice, startX, startY, endX, endY);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

    // 내 글 목록 조회
    @GetMapping("/user/request/list")
    public ResponseEntity myRequestList(
            Authentication authentication,
            @RequestParam(value="page", required = false) Integer page,
            @RequestParam(value="states", required = false) int[] states
    ) {
        try {
            Page<RequestPostListInfoDto> result = requestService.myRequestList(
                    page, (Long)authentication.getPrincipal(), states);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    // 신고 수 10개 이상 글 목록 조회
    @GetMapping("/admin/request/list")
    public ResponseEntity getReportedRequestList(
            @RequestParam(value="page", required = false) Integer page
    ) {
        try {
            Page<RequestPostListInfoDto> result = requestService.getReportedRequestList(page);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
	
	// 글 조희 by requestId 
	@GetMapping("/request/{id}")
	public ResponseEntity viewRequestPost(@PathVariable("id")BigInteger id) {
		logger.info("Passing RequestController -> viewRequestPost");

        try {
            RequestPostInfoDto info = requestService.findRequest(id);
            return new ResponseEntity(info, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	// 글 삭제
	@DeleteMapping("/user/request/{id}")
	public ResponseEntity deleteAllRequestPost(Authentication authentication, 
		@PathVariable("id") BigInteger id) {
		logger.info("Passing RequestController -> deleteAllRequestPost");
		logger.info("DELETE getPrincipal: " + authentication.getPrincipal());

        try {
            requestService.deleteRequest((Long)authentication.getPrincipal(),id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	// 글 수정 
	@PutMapping("/user/request/{id}")
	public ResponseEntity updateRequestPost(Authentication authentication, 
			@PathVariable("id") BigInteger id,
			@ModelAttribute @Valid RequestPostModDto upDto) {
        try {
            requestService.updateRequest((Long)authentication.getPrincipal(), id, upDto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

    @PutMapping("/user/request/{id}/complete")
    public ResponseEntity setComplete(
            Authentication authentication,
            @PathVariable("id") BigInteger id) {
        try {
            requestService.setComplete((Long)authentication.getPrincipal(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/user/request/{id}/posted")
    public ResponseEntity setPosted(
            Authentication authentication,
            @PathVariable("id") BigInteger id) {
        try {
            requestService.setPosted((Long)authentication.getPrincipal(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/user/request/{id}/refresh")
    public ResponseEntity refresh(
            Authentication authentication,
            @PathVariable("id") BigInteger id) {
        try {
            requestService.refresh((Long)authentication.getPrincipal(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PostMapping("/user/request/{id}/report")
    public ResponseEntity report(
            Authentication authentication,
            @PathVariable("id") BigInteger id) {
        try {
            NotificationMember noti = requestService.report((Long)authentication.getPrincipal(), id);
            if(noti != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/admin/request/{id}/ignore")
    public ResponseEntity ignoreReport(
            @PathVariable("id") BigInteger id) {
        try {
            requestService.setIgnoreReport(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/admin/request/{id}/block")
    public ResponseEntity block(
            @PathVariable("id") BigInteger id) {
        try {
            NotificationMember noti = requestService.setBlock(id);
            if(noti != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
