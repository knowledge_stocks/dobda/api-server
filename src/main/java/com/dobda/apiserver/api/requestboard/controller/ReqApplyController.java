package com.dobda.apiserver.api.requestboard.controller;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyAddDto;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyChooseDto;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyModDto;
import com.dobda.apiserver.api.requestboard.dto.RequestApplyInfo;
import com.dobda.apiserver.api.requestboard.service.ReqApplyService;
import com.dobda.apiserver.api.requestboard.service.RequestService;
import com.dobda.apiserver.security.MemberRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;

@RestController
@RequestMapping("/api")
public class ReqApplyController {

	private final Logger logger = LoggerFactory.getLogger(RequestService.class);

	@Autowired
	private ReqApplyService reqApplyService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

	/*
	 * 의뢰 신청
	 * */
    @PostMapping("/helper/requestApply")
    public ResponseEntity applyToRequestPost(
            Authentication authentication,
            @ModelAttribute @Valid RequestApplyAddDto dto
    ) {
        try {
            RequestApplyInfo result = reqApplyService.applyToRequest((Long) authentication.getPrincipal(), dto);
            if (result != null && result.getRequestInfo().getWriterInfo() != null) {
                simpMessagingTemplate.convertAndSend(
                        "/ws/topic/" + result.getRequestInfo().getWriterInfo().getId() + "/noti", "");
                return new ResponseEntity(result, HttpStatus.OK);
            }
            return new ResponseEntity("중복", HttpStatus.BAD_REQUEST);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

	/*
	 * 사용자가 지원한 의뢰
	 * */
	@PutMapping("/helper/requestApply")
	public ResponseEntity changeApplyRequest(
            Authentication authentication,
            @ModelAttribute @Valid RequestApplyModDto dto) {
        try {
            RequestApplyInfo result = reqApplyService.updateReqApply((Long) authentication.getPrincipal(), dto);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

	/*
	 * 의뢰 상태 확인
	 * */
	@GetMapping("/requestApply/{id}")
	public ResponseEntity viewApplyRequest(
            @PathVariable("id")BigInteger reqId) {
        try {
            RequestApplyInfo result = reqApplyService.viewReqApply(reqId);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

	/*
	 * 의뢰 수락
	 * */
	@PutMapping("/user/requestApply/{id}/approve")
	public ResponseEntity setApplyStatus(
            Authentication authentication,
            @PathVariable("id")BigInteger applyId,
            @ModelAttribute @Valid RequestApplyChooseDto dto) {
        try {
            NotificationMember noti = reqApplyService.chooseReqApply((Long) authentication.getPrincipal(), applyId, dto);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

    /*
     * 제안 거부
     * */
    @PutMapping("/user/requestApply/{id}/reject")
    public ResponseEntity reject(
            Authentication authentication,
            @PathVariable("id")BigInteger applyId) {
        try {
            NotificationMember noti = reqApplyService.rejectReqApply((Long) authentication.getPrincipal(), applyId);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + noti.getMemberUser().getId() + "/noti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 제안 삭제
     * */
    @DeleteMapping("/helper/requestApply/{id}")
    public ResponseEntity delete(
            Authentication authentication,
            @PathVariable("id")BigInteger applyId) {
        try {
            reqApplyService.deleteReqApply((Long) authentication.getPrincipal(), applyId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 사용자가 올린 의뢰에대한 지원자 리스트 출력
     */
    @GetMapping("/requestApply/request/{id}")
    public ResponseEntity findUserApplyList(
            Authentication authentication,
            @PathVariable("id") BigInteger requestId,
            @RequestParam(value = "page", required = false) Integer page) {
        try {
            Long memberId = (Long) authentication.getPrincipal();
            boolean isAdmin = authentication.getAuthorities().stream()
                    .filter(auth -> auth.getAuthority().equals(MemberRoles.ADMIN.getFullRole()))
                    .count() > 0;
            if(isAdmin) {
                memberId = null;
            }
            Page<RequestApplyInfo> result = reqApplyService.findApplyList(page, requestId, memberId);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/user/requestApply")
    public ResponseEntity findAcceptApply(
            Authentication authentication,
            @RequestParam(value = "page", required = false) Integer page) {
        try {
            Page<RequestApplyInfo> result = reqApplyService.myAcceptApplyList(page, (Long) authentication.getPrincipal());
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/helper/requestApply")
    public ResponseEntity findUserApplyHelperList(
            Authentication authentication,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value="states", required = false) int[] states
    ) {
        try {
            logger.info("memberId: " + authentication.getPrincipal());
            Page<RequestApplyInfo> result = reqApplyService.findApplyHelperList(page, (Long)authentication.getPrincipal(), states);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
