package com.dobda.apiserver.api.requestboard.entity;

import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.common.enums.ReqAplyState;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;

@Entity
@Table(
        name = "request_apply",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"request_id", "helper_id"})
        })
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
public class ReqApply{
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "BIGINT UNSIGNED")
	private BigInteger id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", nullable = false)
    private Request request;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "helper_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private MemberHelper memberHelper;

	// 지원 내용 (예: 청소가능, 배달 가능)
	@Size(max = 200)
    @Column(nullable = false)
	private String description;

	// 제시한 보수
	@Column(columnDefinition = "INT UNSIGNED", nullable = false)
	private Long amount;
	
	// 지원 등록일
    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;
    
    // 지원 등록변경일
    @UpdateTimestamp
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime modTime;
	
	// 지원 상태
	@Column(columnDefinition = "TINYINT UNSIGNED", nullable = false)
	private ReqAplyState state = ReqAplyState.WAITING;

	@Builder
    public ReqApply(Request request, MemberHelper memberHelper, String description, Long amount) {
        this.request = request;
        this.memberHelper = memberHelper;
        this.description = description;
        this.amount = amount;
    }
}
