package com.dobda.apiserver.api.requestboard.service;

import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.ForbiddenException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.NotificationMemberRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.requestboard.dto.RequestPostAddDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostInfoDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostListInfoDto;
import com.dobda.apiserver.api.requestboard.dto.RequestPostModDto;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.api.requestboard.entity.RequestReport;
import com.dobda.apiserver.api.requestboard.entity.RequestReportKey;
import com.dobda.apiserver.api.requestboard.repository.RequestReportRepository;
import com.dobda.apiserver.api.requestboard.repository.RequestRepository;
import com.dobda.apiserver.common.enums.RequestCategory;
import com.dobda.apiserver.common.enums.RequestState;
import com.dobda.apiserver.common.enums.ResourceType;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RequestService {

	private final Logger logger = LoggerFactory.getLogger(RequestService.class);

	private final RequestRepository requestRepository;
    private final RequestReportRepository reportRepository;
    private final MemberUserRepository memberUserRepository;
    private final NotificationMemberRepository notificationMemberRepository;

	// 게시글 등록
	@Transactional
	public BigInteger addRequest(Long memberUserId, RequestPostAddDto addDto) {
		logger.info("Passing RequestService -> addRequest ");

        Optional<MemberUser> findUser = memberUserRepository.findById(memberUserId);
        if(findUser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
		Request request = Request.builder()
                .memberUser(findUser.get())
                .title(addDto.getRequestTitle())
                .category(RequestCategory.fromCode(addDto.getCategory()))
                .description(addDto.getDescription())
                .payment(addDto.getPayment())
                .isCanSuggestPayment(addDto.getIsCanSuggestPayment())
                .deadline(addDto.getDeadline())
                .address1(addDto.getAddress1())
                .address2(addDto.getAddress2())
                .sidoCode(addDto.getSidoCode())
                .sigunguCode(addDto.getSigunguCode())
                .dongCode(addDto.getDongCode())
                .axisX(addDto.getAxisX())
                .axisY(addDto.getAxisY())
                .build();
		requestRepository.save(request);

        return request.getId();
	}

	/*
	 * 게시글 검색 By 제목
	 * */
	public Page<RequestPostListInfoDto> searchRequest(
            Integer page, String sort, String keyword, Boolean onlyWaiting, int[] categories, Long minPrice, Long maxPrice,
            Double startX, Double startY, Double endX, Double endY
    ){
        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = null;
        if(sort == null) {
            sort = "rc";
        }
        switch(sort) {
            case "up": { // 가격 오름차순
                pageable = PageRequest.of(pageNum, 25, Sort.Direction.ASC, "payment");
                break;
            }
            case "dp": { // 가격 내림차순
                pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "payment");
                break;
            }
            case "rc": // 최근순
            default: {
                pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "sortTime");
                break;
            }
        }

        EnumSet<RequestState> states = EnumSet.of(RequestState.POSTED);
        if(onlyWaiting == null || !onlyWaiting) {
            states.add(RequestState.APPLIED);
        }

        EnumSet<RequestCategory> requestCategories =  EnumSet.noneOf(RequestCategory.class);
        if(categories != null) {
            for (int code: categories) {
                RequestCategory category = RequestCategory.fromCode(code);
                if(category != null) {
                    requestCategories.add(category);
                }
            }
            if(requestCategories.size() == 0) {
                requestCategories = null;
            }
        }

        Page<Request> findResults = requestRepository.findByQuery(
                states, requestCategories, minPrice, maxPrice, keyword, startX, startY, endX, endY, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(RequestPostListInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
	}

    public Page<RequestPostListInfoDto> myRequestList(Integer page, Long memberId, int[] states){
        EnumSet<RequestState> stateSet = EnumSet.noneOf(RequestState.class);
        if(states != null) {
            for (int code: states) {
                RequestState state = RequestState.fromCode(code);
                if(state != null &&
                    state != RequestState.BLOCK &&
                    state != RequestState.DELETED) {
                    stateSet.add(state);
                }
            }
        }
        if(stateSet.size() == 0) {
            stateSet.add(RequestState.POSTED);
            stateSet.add(RequestState.APPLIED);
            stateSet.add(RequestState.COMPLETE);
        }

        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "sortTime");
        Page<Request> findResults = requestRepository.findByMemberUser_IdAndStateIn(memberId, stateSet, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(RequestPostListInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    public Page<RequestPostListInfoDto> getReportedRequestList(Integer page){
        EnumSet<RequestState> stateSet = EnumSet.noneOf(RequestState.class);
        stateSet.add(RequestState.POSTED);
        stateSet.add(RequestState.APPLIED);
        stateSet.add(RequestState.COMPLETE);

        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.DESC, "reportCount");
        Page<Request> findResults = requestRepository.findByIgnoreReportFalseAndReportCountGreaterThanEqualAndStateIn(10L, stateSet, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(RequestPostListInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

	// 게시글 조회
    @Transactional
	public RequestPostInfoDto findRequest(BigInteger id) {
		Optional<Request> findRequest = requestRepository.findById(id);
		if(findRequest.isEmpty()) {
			throw new NotFoundException("항목을 찾을 수 없습니다.");
		}
        Request request = findRequest.get();

        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글입니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 게시글입니다.");
        }

        request.setViewCount(request.getViewCount() + 1);
		return RequestPostInfoDto.fromEntity(request);
	}

	/*
	 * 게시글 삭제(DB에서 삭제가아닌 State를 Delete로 변경)
	 * */
	@Transactional
	public void deleteRequest(Long memberId, BigInteger requestId) {
		logger.info("Passing RequestService -> deleteRequest ");
		Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

		if(!request.getMemberUser().getId().equals(memberId)) {
			logger.info("RequestService: deleteRequest");
			logger.info("MemberId does not match");
			throw new ForbiddenException("권한이 없습니다.");
		}
		logger.info("Service" + request.getState());
		if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글은 수정할 수 없습니다.");
		}
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("거래중인 게시글은 수정할 수 없습니다.");
        }
		request.setState(RequestState.DELETED);
	}

	@Transactional
	public void updateRequest(Long memberId, BigInteger requestId, RequestPostModDto upDto) {
		logger.info("Passing RequestService -> updateRequest ");
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

		if(!request.getMemberUser().getId().equals(memberId)) {
			logger.info("RequestService: updateRequest");
			logger.info("MemberId does not match");
            throw new ForbiddenException("권한이 없습니다.");
		}

        logger.info("Service" + request.getState());
        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("거래중인 게시글은 수정할 수 없습니다.");
        }

        request.setTitle(upDto.getRequestTitle());
        request.setCategory(RequestCategory.fromCode(upDto.getCategory()));
        request.setDescription(upDto.getDescription());
        request.setPayment(upDto.getPayment());
        request.setIsCanSuggestPayment(upDto.getIsCanSuggestPayment());
        request.setDeadline(upDto.getDeadline());
        request.setAddress1(upDto.getAddress1());
        request.setAddress2(upDto.getAddress2());
        request.setSidoCode(upDto.getSidoCode());
        request.setSigunguCode(upDto.getSigunguCode());
        request.setDongCode(upDto.getDongCode());
        request.setAxisX(upDto.getAxisX());
        request.setAxisY(upDto.getAxisY());
        request.setModTime(ZonedDateTime.now());
        request.setIgnoreReport(false);
	}

    @Transactional
    public void setComplete(Long memberId, BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        if(!request.getMemberUser().getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }

        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("거래중인 게시글은 수정할 수 없습니다.");
        }

        request.setState(RequestState.COMPLETE);
    }

    @Transactional
    public void setPosted(Long memberId, BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        if(!request.getMemberUser().getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }

        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("거래중인 게시글은 수정할 수 없습니다.");
        }

        request.setState(RequestState.POSTED);
    }

    // 끌어올리기
    @Transactional
    public void refresh(Long memberId, BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        if(!request.getMemberUser().getId().equals(memberId)) {
            throw new ForbiddenException("권한이 없습니다.");
        }

        if(request.getState() == RequestState.BLOCK) {
            throw new BadRequestException("제재된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.DELETED) {
            throw new BadRequestException("삭제된 게시글은 수정할 수 없습니다.");
        }
        if(request.getState() == RequestState.APPLIED) {
            throw new BadRequestException("거래중인 게시글은 수정할 수 없습니다.");
        }

        if(request.getSortTime().isAfter(ZonedDateTime.now().minusDays(1))) {
            throw new BadRequestException("끌어올린지 최소 하루가 지나야 다시 끌어올릴 수 있습니다.");
        }
        request.setSortTime(ZonedDateTime.now());
    }

    @Transactional()
    public NotificationMember report(Long memberId, BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        Optional<MemberUser> findMember = memberUserRepository.findById(memberId);
        if(findMember.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findMember.get();

        Optional<RequestReport> findReport = reportRepository.findById(new RequestReportKey(requestId, memberId));
        if(!findReport.isEmpty()) {
            throw new BadRequestException("이미 신고한 글입니다.");
        }

        RequestReport newReport = RequestReport.builder()
                .request(request)
                .memberUser(memberUser)
                .build();
        reportRepository.save(newReport);

        request.setReportCount(request.getReportCount() + 1);
        if(request.getState() != RequestState.BLOCK)
        {
            // 관리자 알림은 누구에게 배정할지가 애매하니 일단 패스
//            if(request.getReportCount() == 10) {
//                simpMessagingTemplate.convertAndSend(
//                        "/ws/topic/admin/readchat/" + roomId, roomId);
//            }
            // 신고 20개 누적시 강제 블록
            if(request.getReportCount() >= 20) {
                request.setState(RequestState.BLOCK);

                if(request.getMemberUser() != null) {
                    NotificationMember newNoti = NotificationMember.builder()
                            .memberUser(request.getMemberUser())
                            .message("'" + request.getTitle() + "' 의뢰글이 신고 누적되어 제재되었습니다.")
                            .targetType(ResourceType.REQUEST)
                            .targetId(request.getId())
                            .build();
                    notificationMemberRepository.save(newNoti);

                    return newNoti;
                }
            } else {
                if(request.getMemberUser() != null) {
                    NotificationMember newNoti = NotificationMember.builder()
                            .memberUser(request.getMemberUser())
                            .message("'" + request.getTitle() + "' 의뢰글에 신고가 접수되었습니다. 현재 " + request.getReportCount() + "회 / 20회 누적될 경우 제재됩니다.")
                            .targetType(ResourceType.REQUEST)
                            .targetId(request.getId())
                            .build();
                    notificationMemberRepository.save(newNoti);

                    return newNoti;
                }
            }
        }
        return null;
    }

    // 관리자 영역
    @Transactional
    public void setIgnoreReport(BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        request.setIgnoreReport(true);
    }

    @Transactional()
    public NotificationMember setBlock(BigInteger requestId) {
        Optional<Request> findRequest = requestRepository.findById(requestId);
        if(findRequest.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        Request request = findRequest.get();

        if(request.getState() != RequestState.BLOCK) {
            request.setState(RequestState.BLOCK);

            if(request.getMemberUser() != null) {
                NotificationMember newNoti = NotificationMember.builder()
                        .memberUser(request.getMemberUser())
                        .message("'" + request.getTitle() + "' 의뢰글이 관리자에 의해 제재되었습니다.")
                        .targetType(ResourceType.REQUEST)
                        .targetId(request.getId())
                        .build();
                notificationMemberRepository.save(newNoti);
                return newNoti;
            }
        }
        return null;
    }
}
