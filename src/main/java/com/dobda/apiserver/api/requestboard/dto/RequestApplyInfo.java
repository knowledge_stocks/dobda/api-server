package com.dobda.apiserver.api.requestboard.dto;

import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.requestboard.entity.ReqApply;
import com.dobda.apiserver.common.enums.ReqAplyState;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class RequestApplyInfo {
	// 의뢰신청 조회용

    private BigInteger id;
    private RequestPostListInfoDto requestInfo;
    private MemberPublicInfo helperInfo;
	private String description;
	private Long amount;
	private ReqAplyState state;
    private ZonedDateTime regTime;
    private ZonedDateTime modTime;
	
	public static RequestApplyInfo fromEntity(ReqApply entity) {
        if(entity == null) {
            return null;
        }
		return builder()
				.id(entity.getId())
                .requestInfo(RequestPostListInfoDto.fromEntity(entity.getRequest()))
                .helperInfo(entity.getMemberHelper() != null
                        ? MemberPublicInfo.fromEntity(entity.getMemberHelper().getMemberUser())
                        : null)
                .description(entity.getDescription())
                .amount(entity.getAmount())
                .state(entity.getState())
                .regTime(entity.getRegTime())
                .modTime(entity.getModTime())
				.build();
	}
}
