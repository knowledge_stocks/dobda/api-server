package com.dobda.apiserver.api.requestboard.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Data
public class RequestApplyModDto {

    @NotNull
    private BigInteger requestId;
    @NotNull
    private String description;
    @NotNull
    private Long amount;
}
