package com.dobda.apiserver.api.chat.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
public class ChattingMsg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "BIGINT UNSIGNED")
    private BigInteger id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ChattingRoom chattingRoom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id")
    private MemberUser memberUser;

    @Size(max = 200)
    @Column(nullable = false)
    private String message;

    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;

    @Builder
    public ChattingMsg(ChattingRoom chattingRoom, MemberUser memberUser, String message) {
        this.chattingRoom = chattingRoom;
        this.memberUser = memberUser;
        this.message = message;
    }
}
