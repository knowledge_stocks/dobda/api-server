package com.dobda.apiserver.api.chat;

import com.dobda.apiserver.api.chat.entity.ChattingRoom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface ChattingRoomRepository extends JpaRepository<ChattingRoom, BigInteger> {
}
