package com.dobda.apiserver.api.chat.dto;

import com.dobda.apiserver.api.chat.entity.ChattingRoomMember;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class ChattingRoomInfoDto {

    private BigInteger id;
    private Long partnerId;
    private String partnerNickname;
    private String lastMessage;
    private ZonedDateTime lastTime;
    private Boolean hasUnreadMsg;

    public static ChattingRoomInfoDto fromEntity(ChattingRoomMember entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .id(entity.getChattingRoom().getId())
                .partnerId(entity.getPartner().getId())
                .partnerNickname(entity.getPartner().getNickname())
                .lastMessage(entity.getChattingRoom().getLastMessage())
                .lastTime(entity.getChattingRoom().getLastTime())
                .hasUnreadMsg(entity.getHasUnreadMsg())
                .build();
    }
}
