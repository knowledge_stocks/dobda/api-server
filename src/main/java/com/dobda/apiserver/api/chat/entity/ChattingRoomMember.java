package com.dobda.apiserver.api.chat.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
@IdClass(ChattingRoomMemberKey.class)
public class ChattingRoomMember {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id", columnDefinition = "BIGINT UNSIGNED", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ChattingRoom chattingRoom;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", columnDefinition = "INT UNSIGNED", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MemberUser memberUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "partner_id", columnDefinition = "INT UNSIGNED")
    private MemberUser partner;

    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean hasUnreadMsg = false;

    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean isHide = false;

    @Builder
    public ChattingRoomMember(ChattingRoom chattingRoom, MemberUser memberUser, MemberUser partner) {
        this.chattingRoom = chattingRoom;
        this.memberUser = memberUser;
        this.partner = partner;
    }
}
