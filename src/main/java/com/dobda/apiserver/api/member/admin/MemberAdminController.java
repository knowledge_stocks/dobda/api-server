package com.dobda.apiserver.api.member.admin;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.admin.dto.MemberAdminAddDTO;
import com.dobda.apiserver.api.member.admin.dto.MemberAdminNewPwd;
import com.dobda.apiserver.api.member.admin.dto.MemberAdminPublicInfo;
import com.dobda.apiserver.api.member.admin.dto.NotificationAdminInfoDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.math.BigInteger;


@RestController
@Slf4j
public class MemberAdminController {
	
	@Autowired
	private MemberAdminService memberAdminService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

	/*
	 * 관리자 생성. 첫 관리자일 때만 권한 요구 없이 생성 가능
	 */
	@PostMapping("/api/admin/add")
	public ResponseEntity add(
            Authentication authentication,
            @ModelAttribute MemberAdminAddDTO dto
    ) {
        try {
            memberAdminService.add(authentication, dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}
	
	/*
	 * 이메일 중복체크
	 */
	@GetMapping("/api/public/adminEmailChk")
	public ResponseEntity<Boolean> emailDuplicateChk(@RequestParam("email") String email) {
		if(email.isEmpty() || email.trim().equals("")) {
			return new ResponseEntity("이메일은 필수 입력 사항입니다.", HttpStatus.BAD_REQUEST);
		}
		
		boolean emailChk = memberAdminService.emailDuplicate(email);
		return ResponseEntity.ok(emailChk);
	}
	
	/*
	 * 닉네임 중복체크
	 */
    @GetMapping("/api/public/adminNicknameChk")
	public ResponseEntity<Boolean> nickNameDuplicateChk(@RequestParam("nickname") String nickname) {
		if(nickname.isEmpty() || nickname.trim().equals("")) {
			return new ResponseEntity("닉네임은 필수 입력 사항입니다.", HttpStatus.BAD_REQUEST);
		}
		
		boolean nickNameChk = memberAdminService.nickNameDuplicate(nickname);
		return ResponseEntity.ok(nickNameChk);
	}

    /*
     * 전화번호 중복체크
     */
    @GetMapping("/api/public/adminPhoneNumChk")
    public ResponseEntity<Boolean> phoneNumDuplicateChk(@RequestParam("phoneNum") String phoneNum) {
        if(phoneNum.isEmpty() || phoneNum.trim().equals("")) {
            return new ResponseEntity("전화번호는 필수 입력 사항입니다.", HttpStatus.BAD_REQUEST);
        }

        boolean phoneNumChk = memberAdminService.phoneNumDuplicate(phoneNum);
        return ResponseEntity.ok(phoneNumChk);
    }

    @DeleteMapping("/api/admin")
    public ResponseEntity remove(
            Authentication authentication,
            @RequestHeader("pwd") String pwd
    ) {
        try {
            memberAdminService.removeMember((Long) authentication.getPrincipal(), pwd);
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/api/public/admin/{id}")
    public ResponseEntity info(@PathVariable("id") Long id) {
        try {
            MemberAdminPublicInfo result = memberAdminService.getMemberPublicInfo(id);
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
    
    //비밀번호 변경 
    @PutMapping("/api/admin/changePwd")
    public ResponseEntity changePwd(
            Authentication authentication,
            @ModelAttribute @Valid MemberAdminNewPwd dto) {
    	log.info("changePwd 진입");
        try {
            memberAdminService.changePwd((Long)authentication.getPrincipal(), dto.getPwd(), dto.getNewPwd());
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 이메일 변경
     * */
    @PutMapping("/api/admin/emailChange")
    public ResponseEntity changeUserEmail(
            Authentication authentication,
            @RequestParam("email") @Size(min = 3) @Valid String email
    ) {
        try {
            memberAdminService.changeEmail((Long)authentication.getPrincipal(), email);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 휴대폰 번호 변경
     * 변경전 인증 절차 필요
     * */
    @PutMapping("/api/admin/phoneNumberChange")
    public ResponseEntity changeUserPhoneNumber(
            Authentication authentication,
            @RequestParam("phoneNum") String phoneNum,
            @RequestParam("pwd") String pwd
    ) {
        try {
            memberAdminService.changePhoneNumber((Long)authentication.getPrincipal(), phoneNum, pwd);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    /*
     * 닉네임 변겅
     * 닉네임 중복 확인후 변경
     * */
    @PutMapping("/api/admin/nickChange")
    public ResponseEntity changeNickName(
            Authentication authentication,
            @RequestParam("newNickName") @Size(min = 3) @Valid String newNickName){
        try {
            memberAdminService.changeNickName((Long)authentication.getPrincipal(), newNickName);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    // 알림 관련 api
    @PutMapping("/api/admin/noti/read")
    public ResponseEntity checkRead(
            Authentication authentication) {
        try {
            memberAdminService.checkRead((Long) authentication.getPrincipal());
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/admin/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/api/admin/noti/list")
    public ResponseEntity getNotiList(
            Authentication authentication,
            @RequestParam(value = "offsetId", required = false) BigInteger offsetId
    ) {
        try {
            Page<NotificationAdminInfoDto> result =
                    memberAdminService.getNotiList((Long) authentication.getPrincipal(), offsetId);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/admin/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @DeleteMapping("/api/admin/noti")
    public ResponseEntity clearNoti(
            Authentication authentication
    ) {
        try {
            memberAdminService.clearNotiList((Long)authentication.getPrincipal());
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/admin/" + authentication.getPrincipal() + "/readnoti", "");
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @DeleteMapping("/api/admin/noti/{id}")
    public ResponseEntity removeNoti(
            Authentication authentication,
            @PathVariable("id") BigInteger id
    ) {
        try {
            memberAdminService.removeNoti((Long)authentication.getPrincipal(),id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
