package com.dobda.apiserver.api.member.admin.dto;

import com.dobda.apiserver.api.member.admin.entity.MemberAdmin;
import com.dobda.apiserver.common.enums.AdminLevel;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class MemberAdminPublicInfo {
    private Boolean isAdmin;
    private Long id;
    private String nickname;
    private Boolean isMaster;
    private ZonedDateTime regTime;

    public static MemberAdminPublicInfo fromEntity(MemberAdmin entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .isAdmin(true)
                .id(entity.getId())
                .nickname(entity.getNickname())
                .isMaster(entity.getLevel() == AdminLevel.MASTER)
                .regTime(entity.getRegTime())
                .build();
    }
}
