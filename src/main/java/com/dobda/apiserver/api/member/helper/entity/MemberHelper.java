package com.dobda.apiserver.api.member.helper.entity;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.entity.ReqApply;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Size;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@DynamicInsert
@DynamicUpdate
@Table(name = "member_helper")
@NoArgsConstructor
public class MemberHelper {

    @Id
    private Long id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id", columnDefinition = "INT UNSIGNED")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MemberUser memberUser;

    @Column(columnDefinition = "INT UNSIGNED DEFAULT 0", nullable = false)
    private Long totalHelpCnt;            //총 의뢰 접수 횟수

    @Column(columnDefinition = "INT UNSIGNED DEFAULT 0", nullable = false)
    private Long totalCompleteCnt;        //총 의뢰 완료 횟수

    @Column(columnDefinition = "INT UNSIGNED DEFAULT 0", nullable = false)
    private Long totalSuccessCnt;        //총 의뢰 성공 횟수

    @Size(max=128)
    @Column(nullable = false)
    private String address;

    @Size(max = 50)
    @Column(nullable = false)
    private String bankName;
    
    @Size(max=24)
    @Column(nullable = false)
    private String fintechNum;

    @CreatedDate
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    private ZonedDateTime regTime;
    
    @Builder
    public MemberHelper(MemberUser memberUser, String address, String bankName, String fintechNum) {
        this.memberUser = memberUser;
        this.address = address;
        this.bankName = bankName;
        this.fintechNum = fintechNum;
    }

    @OneToMany(mappedBy = "memberHelper", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ReqApply> reqApplyList = new ArrayList<>();

    @OneToMany(mappedBy = "memberHelper")
    private List<Order> orderList = new ArrayList<>();

    @PreRemove
    private void preRemove() {
        orderList.forEach(v -> v.setMemberHelper(null));
        orderList.clear();
    }
}
