package com.dobda.apiserver.api.member.user.dto;

import com.dobda.apiserver.api.member.helper.dto.MemberHelperInfoDto;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.common.enums.MemberUserLevel;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class MemberPublicInfo {
    private Boolean isAdmin = false;
    private Long id;
    private String nickname;
    private Boolean isHelper;
    private String introduction;
    private ZonedDateTime regTime;
    private ZonedDateTime denyTime;
    private Double trustPoint;
    private Long totalRequestCount;
    private Long totalReceivedReportCount;
    private MemberHelperInfoDto helperInfo;

    public static MemberPublicInfo fromEntity(MemberUser entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .isAdmin(false)
                .id(entity.getId())
                .nickname(entity.getNickname())
                .isHelper(entity.getLevel() == MemberUserLevel.HELPER)
                .introduction(entity.getIntroduction())
                .regTime(entity.getRegTime())
                .denyTime(entity.getDenyTime())
                .trustPoint(entity.getTrustPoint())
                .totalRequestCount(entity.getTotalRequestCount())
                .totalReceivedReportCount(entity.getTotalReceivedReportCount())
                .helperInfo(MemberHelperInfoDto.fromEntity(entity.getMemberHelper()))
                .build();
    }
}
