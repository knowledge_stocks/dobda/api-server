package com.dobda.apiserver.api.member.helper;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.member.helper.dto.HelperApplyInfo;
import com.dobda.apiserver.api.member.helper.dto.HelperApplyStateInfo;
import com.dobda.apiserver.api.member.helper.dto.MemberHelperApplyDTO;
import com.dobda.apiserver.api.order.dto.RetryRefundAllDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@RestController
@Slf4j
@Validated
public class MemberHelperController {
	
	@Autowired
	private MemberHelperService memberHelperService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

	/*
	 *	헬퍼 지원 (일반 사용자)
	 */
	@PostMapping("/api/user/helperApply")
	public ResponseEntity applyHelper(
            Authentication authentication,
            @ModelAttribute @Valid MemberHelperApplyDTO dto) {
		try {
            memberHelperService.applyHelper((Long)authentication.getPrincipal(), dto);
			return new ResponseEntity(HttpStatus.OK);
		}catch(BaseException e) {
			return new ResponseEntity(e.getMessage(), e.getStatusCode());
		}
	}
	
	/*
	 *	헬퍼 지원 상태 조회 (사용자) 
	 */
	@GetMapping("/api/user/helperApply/state")
	public ResponseEntity helperApplyState (Authentication authentication) {
		try {
			Long id = (Long)authentication.getPrincipal();
			HelperApplyStateInfo stateInfo = memberHelperService.getHelperApplyState(id);
		
			return new ResponseEntity(stateInfo, HttpStatus.OK);
		}catch(BaseException e) {
			return new ResponseEntity(e.getMessage(), e.getStatusCode());
		}
	}

	/*
	 * 승인 전 헬퍼 지원 취소 (사용자)
	 */
	@DeleteMapping("/api/user/helperApply")
	public ResponseEntity cancelApply(Authentication authentication) {
		try {
			memberHelperService.cancelApply((Long)authentication.getPrincipal());
			return new ResponseEntity(HttpStatus.OK);
		}catch (BaseException e) {
			return new ResponseEntity(e.getMessage(), e.getStatusCode());
		}
	}
	
	
	/*
     *  헬퍼 지원 회원 목록 조회 (관리자 담당)
     */
    @GetMapping("/api/admin/helperApplys")
    public ResponseEntity getHelperApplicants(
            @RequestParam(value="page", required = false) Integer page
    ) {
    	try {
    		Page<HelperApplyInfo> helperApplyList = memberHelperService.findAllHelperApplicants(page);
    		return new ResponseEntity(helperApplyList, HttpStatus.OK);
    	}catch (BaseException e) {
    		return new ResponseEntity(e.getMessage(), e.getStatusCode());
    	}
    }

    /*
     * 헬퍼 지원자 승인 (관리자 담당) 
     */
    @PutMapping("/api/admin/helperApply/{id}/approve")
    public ResponseEntity approveHelperApply(@PathVariable("id") Long id) {
    	try {
    		memberHelperService.approveHelperApply(id);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + id + "/noti", "");
    		return new ResponseEntity(HttpStatus.OK);
    	}catch(BaseException e) {
    		return new ResponseEntity(e.getMessage(), e.getStatusCode());
    	}
    }
    
    
	/*
	 * 헬퍼 지원자 승인 거절 (관리자 담당) 
	 */
    @PutMapping("/api/admin/helperApply/{id}/reject")
    public ResponseEntity rejectHelperApply(@PathVariable("id") Long id) {
    	try {
    		memberHelperService.rejectHelperApply(id);
            simpMessagingTemplate.convertAndSend(
                    "/ws/topic/" + id + "/noti", "");
    		return new ResponseEntity(HttpStatus.OK);
    	}catch(BaseException e) {
    		return new ResponseEntity(e.getMessage(), e.getStatusCode());
    	}
    }

    /*
     * 헬퍼 탈퇴 (사용자 권한은 유지) 
     */
    @DeleteMapping("/api/helper")
    public ResponseEntity removeHelper(
    		Authentication authentication,
            @RequestHeader("pwd") String pwd) {
    	try {
    		memberHelperService.removeHelper((Long)authentication.getPrincipal(), pwd);
    		return new ResponseEntity(HttpStatus.OK);
    	}catch (BaseException e) {
    		return new ResponseEntity(e.getMessage(), e.getStatusCode());
		}
    }

    @PutMapping("/api/helper/banking")
    public ResponseEntity changeBanking(
            Authentication authentication,
            @ModelAttribute RetryRefundAllDto dto
    ){
        try {
            memberHelperService.updateBankInfo((Long)authentication.getPrincipal(), dto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PutMapping("/api/helper/address")
    public ResponseEntity changeAddress(
            Authentication authentication,
            @RequestParam("address") @Size(min = 3) @Valid String address
    ){
        try {
            memberHelperService.updateAddress((Long)authentication.getPrincipal(), address);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}//end class
