package com.dobda.apiserver.api.member.admin.dto;

import com.dobda.apiserver.api.member.admin.entity.NotificationAdmin;
import com.dobda.apiserver.common.enums.ResourceType;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class NotificationAdminInfoDto {
    private BigInteger id;
    private Long adminId;
    private String message;
    private Long sourceMemberId;
    private ResourceType targetType;
    private BigInteger targetId;
    private ZonedDateTime regTime;

    public static NotificationAdminInfoDto fromEntity(NotificationAdmin entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .id(entity.getId())
                .adminId(entity.getMemberAdmin().getId())
                .message(entity.getMessage())
                .sourceMemberId(entity.getSourceMemberId())
                .targetType(entity.getTargetType())
                .targetId(entity.getTargetId())
                .regTime(entity.getRegTime())
                .build();
    }
}
