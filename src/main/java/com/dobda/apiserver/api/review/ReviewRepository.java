package com.dobda.apiserver.api.review;

import com.dobda.apiserver.api.review.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface ReviewRepository extends JpaRepository<Review, BigInteger>{
	
	public Page<Review> findById(Long id, Pageable pagable);
	
	public Page<Review> findByWriter_Id(Long id, Pageable pageable);	
	
	public Page<Review> findByTarget_Id(Long id, Pageable pageable);
	
	public Optional<Review> findByOrder_Id(BigInteger orderId);

    public Review findByWriter_IdAndOrder_Id(Long writerId, BigInteger orderId);
}
