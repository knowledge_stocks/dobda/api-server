package com.dobda.apiserver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "com.dobda")
public class DobdaProperties  implements InitializingBean {
    private String naverSmsServiceId;
    private String naverSmsCallNumber;
    private String naverApiAccessKey;
    private String naverApiSecretKey;
    private String bankClientId;
    private String bankSecret;
    private String bankState;
    private String bankClientCode;
    private String bankPassPhrase;
    private String bankName;
    private String bankAccount;
    private String bankCode;
    private String uploadPath;
    private List<String> allowOrigins;

    private static DobdaProperties instance;

    @Override
    public void afterPropertiesSet() throws Exception {
        instance = this;
    }

    public static DobdaProperties getInstance() {
        return instance;
    }

    @Bean
    public PageableHandlerMethodArgumentResolverCustomizer customize() {
        // JPA Pageable 설정
        return p -> {
            p.setOneIndexedParameters(true);	// 1부터 시작
            p.setMaxPageSize(100);				// size=10
        };
    }

    @Bean
    public TaskExecutor refreshTokenTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(12);
        executor.setMaxPoolSize(12);
        executor.setThreadNamePrefix("refresh_token_thread");
        executor.initialize();
        return executor;
    }
}
