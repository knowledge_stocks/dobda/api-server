package com.dobda.apiserver.common.util.openbank;

import com.dobda.apiserver.common.util.openbank.account.AccountResponse;
import com.dobda.apiserver.common.util.openbank.payment.DepositRequest;
import com.dobda.apiserver.common.util.openbank.payment.DepositResponse;
import com.dobda.apiserver.common.util.openbank.payment.WithdrawRequest;
import com.dobda.apiserver.common.util.openbank.payment.WithdrawResponse;
import com.dobda.apiserver.common.util.openbank.token.*;
import com.dobda.apiserver.config.DobdaProperties;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class OpenBankUtils {

    private final static String baseUrl = "https://testapi.openbanking.or.kr";

    public static UserTokenResponse getUserToken(UserTokenRequest request) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("code", request.getCode());
        parameters.add("client_id", DobdaProperties.getInstance().getBankClientId());
        parameters.add("client_secret", DobdaProperties.getInstance().getBankSecret());
        parameters.add("redirect_uri", request.getRedirectUri());
        parameters.add("grant_type", "authorization_code");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("UTF-8")));

        HttpEntity body = new HttpEntity<>(parameters,headers);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<UserTokenResponse> response = restTemplate.exchange(
                baseUrl + "/oauth/2.0/token", HttpMethod.POST, body, UserTokenResponse.class);

        return response.getBody();
    }

    public static RefreshTokenResponse refreshUserToken(RefreshTokenRequest request) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("client_id", DobdaProperties.getInstance().getBankClientId());
        parameters.add("client_secret", DobdaProperties.getInstance().getBankSecret());
        parameters.add("refresh_token", request.getRefreshToken());
        parameters.add("scope", request.getScope());
        parameters.add("grant_type", "refresh_token");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("UTF-8")));

        HttpEntity body = new HttpEntity<>(parameters,headers);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RefreshTokenResponse> response = restTemplate.exchange(
                baseUrl + "/oauth/2.0/token", HttpMethod.POST, body, RefreshTokenResponse.class);

        return response.getBody();
    }

    public static OrgTokenResponse getOrgToken() {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("client_id", DobdaProperties.getInstance().getBankClientId());
        parameters.add("client_secret", DobdaProperties.getInstance().getBankSecret());
        parameters.add("scope", "oob");
        parameters.add("grant_type", "client_credentials");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("UTF-8")));

        HttpEntity body = new HttpEntity<>(parameters,headers);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<OrgTokenResponse> response = restTemplate.exchange(
                baseUrl + "/oauth/2.0/token", HttpMethod.POST, body, OrgTokenResponse.class);

        return response.getBody();
    }

    public static RevokeTokenResponse revokeToken(String token) {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("client_id", DobdaProperties.getInstance().getBankClientId());
        parameters.add("client_secret", DobdaProperties.getInstance().getBankSecret());
        parameters.add("access_token", token);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("UTF-8")));

        HttpEntity body = new HttpEntity<>(parameters,headers);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<RevokeTokenResponse> response = restTemplate.exchange(
                baseUrl + "/oauth/2.0/revoke", HttpMethod.POST, body, RevokeTokenResponse.class);

        return response.getBody();
    }

    public static AccountResponse getMyInfo(String accessToken, String userSeqNo) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+accessToken);

        HttpEntity body = new HttpEntity<>(headers);

        String url = baseUrl + "/v2.0/user/me";
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("user_seq_no", userSeqNo)
                .build();

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<AccountResponse> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, body, AccountResponse.class);

        return response.getBody();
    }

    public static WithdrawResponse withdraw(String token, String fintech, String accountHolderName, String userSeqNo, Long amount) {
        String url = baseUrl + "/v2.0/transfer/withdraw/fin_num";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+ token);

        WithdrawRequest request = WithdrawRequest.builder()
                .bank_tran_id(genTranId())
                .cntr_account_num(DobdaProperties.getInstance().getBankAccount())
                .fintech_use_num(fintech) // 출금 계좌
                .tran_amt(amount)
                .tran_dtime(ZonedDateTime.now(ZoneId.of("Asia/Seoul")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                .req_client_name(accountHolderName)
                .req_client_fintech_use_num(fintech)
                .req_client_num(userSeqNo)
                .recv_client_name(DobdaProperties.getInstance().getBankName())
                .recv_client_bank_code(DobdaProperties.getInstance().getBankCode())
                .recv_client_account_num(DobdaProperties.getInstance().getBankAccount())
                .build();

        ResponseEntity<WithdrawRequest> data = new ResponseEntity<>(request, headers, HttpStatus.OK);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<WithdrawResponse> response = restTemplate.exchange(url, HttpMethod.POST, data, WithdrawResponse.class);

        return response.getBody();
    }

    public static DepositResponse deposit(String orgToken, String fintech, String accountHolderName, String userSeqNo, Long amount, boolean isRefurn) {
        String url = baseUrl + "/v2.0/transfer/deposit/fin_num";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+ orgToken);

        DepositRequest.Details detail = DepositRequest.Details.builder()
                .bank_tran_id(genTranId())
                .fintech_use_num(fintech)
                .print_content(isRefurn ? "의뢰대금 환불" : "의뢰대금 정산")
                .tran_amt(amount)
                .req_client_name(accountHolderName)
                .req_client_fintech_use_num(fintech)
                .req_client_num(userSeqNo)
                .build();

        DepositRequest depositReq = DepositRequest.builder()
                .cntr_account_num(DobdaProperties.getInstance().getBankAccount())
                .wd_pass_phrase(DobdaProperties.getInstance().getBankPassPhrase())
                .wd_print_content(isRefurn ? "DOBDA 환불" : "DOBDA 정산")
                .tran_dtime(ZonedDateTime.now(ZoneId.of("Asia/Seoul")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                .req_list(detail)
                .build();

        ResponseEntity<DepositRequest> data = new ResponseEntity<>(depositReq, headers, HttpStatus.OK);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<DepositResponse> response = restTemplate.exchange(url, HttpMethod.POST, data, DepositResponse.class);
        return response.getBody();
    }

    private static String genTranId() {
        char data = (char)((Math.random()*26)+65);
        ZonedDateTime nowUTC = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("UTC"));
        String res = nowUTC.format(DateTimeFormatter.ofPattern("ddhhmmss"))+data;

        return DobdaProperties.getInstance().getBankClientCode() + "U" + res;
    }
}
