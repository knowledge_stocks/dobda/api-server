package com.dobda.apiserver.common.util.openbank.token;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RefreshTokenRequest {

    private String refreshToken;
    private String scope;
}
