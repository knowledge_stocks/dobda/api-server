package com.dobda.apiserver.common.util.openbank.account;

import lombok.Data;

import java.util.List;

@Data
public class AccountResponse{
	private String api_tran_id;
    private String api_tran_dtm;
    private String rsp_code; // 성공: "A0000", 사용자 불일치: "A0313", 토큰 거부 "O0002"
    private String rsp_message;
	private String user_seq_no;
	private String user_ci;
	private String user_name;
	private String res_cnt;
	private List<Account> res_list;
		
	@Data
	public static class Account{
		private String fintech_use_num;
		private String account_alias;
		private String bank_code_std;
		private String bank_code_sub;
		private String bank_name;
		private String savings_bank_name;
        private String account_num_masked;
		private String account_seq;
        private String account_holder_name;
        private String account_holder_type;
		private String account_type;
        private String inquiry_agree_yn;
        private String inquiry_agree_dtime;
        private String transfer_agree_yn;
        private String transfer_agree_dtime;
        private String payer_num;
		}
	}
