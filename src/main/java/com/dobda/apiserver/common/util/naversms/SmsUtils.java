package com.dobda.apiserver.common.util.naversms;

import com.dobda.apiserver.config.DobdaProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class SmsUtils {

    public static SmsResponse sendSms(String receiverNum, String text) throws JsonProcessingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, URISyntaxException {
        DobdaProperties dobdaProperties = DobdaProperties.getInstance();

        String serviceId = dobdaProperties.getNaverSmsServiceId();
        String apiAccessKey = dobdaProperties.getNaverApiAccessKey();
        String callNumber = dobdaProperties.getNaverSmsCallNumber();

        List<MessageDto> messages = new ArrayList<>();
        messages.add(new MessageDto(receiverNum.replace("-",""), text));

        SmsRequest smsRequest = new SmsRequest("SMS", "COMM", "82", callNumber, "기본 문자", messages);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonBody = objectMapper.writeValueAsString(smsRequest);

        String time = String.valueOf(System.currentTimeMillis());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-ncp-apigw-timestamp", time);
        headers.set("x-ncp-iam-access-key", apiAccessKey);
        String sig = makeSignature(serviceId, apiAccessKey, time);
        headers.set("x-ncp-apigw-signature-v2", sig);

        HttpEntity<String> body = new HttpEntity<>(jsonBody,headers);

        RestTemplate restTemplate = new RestTemplate();
        SmsResponse smsResponse = restTemplate.postForObject(
                new URI("https://sens.apigw.ntruss.com/sms/v2/services/" + serviceId + "/messages"), body, SmsResponse.class);

        return smsResponse;
    }

    private static String makeSignature(String serviceId, String apiAccessKey, String time) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String apiSecretKey = DobdaProperties.getInstance().getNaverApiSecretKey();

        String space = " ";
        String newLine = "\n";
        String method = "POST";
        String url = "/sms/v2/services/"+ serviceId +"/messages";
        String timestamp = time.toString();
        String accessKey = apiAccessKey;
        String secretKey = apiSecretKey;

        String message = new StringBuilder()
                .append(method)
                .append(space)
                .append(url)
                .append(newLine)
                .append(timestamp)
                .append(newLine)
                .append(accessKey)
                .toString();

        SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(signingKey);

        byte[] rawHmac = mac.doFinal(message.getBytes("UTF-8"));
        String encodeBase64String = Base64.encodeBase64String(rawHmac);

        return encodeBase64String;
    }
}
