package com.dobda.apiserver.common.util.openbank.payment;

import lombok.Builder;
import lombok.Data;

@Data
public class WithdrawRequest {
	
    private String bank_tran_id;  // 기관(DOBDA)에서 부여한 거래 id
    private String cntr_account_type = "N"; // 계좌/계정 구분(고유값 "N")
    private String cntr_account_num; // 기관 약정 계좌
    private String dps_print_content = "의뢰대금"; // 입금 계좌 출력 내역
    private String fintech_use_num; // 출금 계좌 핀테크 번호
    private String wd_print_content = "의뢰대금";
    private Long tran_amt; // 거래금액
    private String tran_dtime; // 거래일시
    private String req_client_name; // 의뢰 고객 이름
    private String req_client_fintech_use_num; // 의뢰 고객 핀테크 번호
    private String req_client_num; // 의뢰 고객 id
    private String transfer_purpose = "TR"; // 이체 용도(고유값 "TR")
    private String recv_client_name; // 기관 예금주명
    private String recv_client_bank_code; // 기관 계좌 은행번호
    private String recv_client_account_num; // 기관 계좌번호
	
    @Builder

    public WithdrawRequest(
            String bank_tran_id, String cntr_account_num, String fintech_use_num,
            Long tran_amt, String tran_dtime, String req_client_name, String req_client_fintech_use_num,
            String req_client_num, String recv_client_name, String recv_client_bank_code, String recv_client_account_num) {
        this.bank_tran_id = bank_tran_id;
        this.cntr_account_num = cntr_account_num;
        this.fintech_use_num = fintech_use_num;
        this.tran_amt = tran_amt;
        this.tran_dtime = tran_dtime;
        this.req_client_name = req_client_name;
        this.req_client_fintech_use_num = req_client_fintech_use_num;
        this.req_client_num = req_client_num;
        this.recv_client_name = recv_client_name;
        this.recv_client_bank_code = recv_client_bank_code;
        this.recv_client_account_num = recv_client_account_num;
    }
}
