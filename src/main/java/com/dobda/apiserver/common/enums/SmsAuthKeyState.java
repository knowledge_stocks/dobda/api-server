package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum SmsAuthKeyState {
    AUTH_WAITING(0), // 회원가입용
    AUTHENTICATED(1), // 회원가입용
    RE_AUTH_WAITING(2), // 회원정보 변경용
    RE_AUTHENTICATED(3); // 회원정보 변경용

    private static final Map<Integer, SmsAuthKeyState> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(SmsAuthKeyState::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private SmsAuthKeyState(int code)
    {
        this.code = code;
    }

    public static SmsAuthKeyState fromCode(int code) {
        SmsAuthKeyState result = intToEnum.get(code);
        return result;
    }
}