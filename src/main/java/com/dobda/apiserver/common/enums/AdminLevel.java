package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum AdminLevel {
    MASTER(100),
    JUNIOR(10);

    private static final Map<Integer, AdminLevel> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(AdminLevel::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private AdminLevel(int code)
    {
        this.code = code;
    }

    public static AdminLevel fromCode(int code) {
        AdminLevel result = intToEnum.get(code);
        return result;
    }
}
