package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum RequestState {
    POSTED(10), // 헬퍼 찾는중
    APPLIED(20), // 지원자 결정, 결제
    COMPLETE(30), // 완료
    DELETED(40), // 삭제됨
    BLOCK(60); // 신고로 제재됨

    private static final Map<Integer, RequestState> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(RequestState::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private RequestState(int code)
    {
        this.code = code;
    }

    public static RequestState fromCode(int code) {
        RequestState result = intToEnum.get(code);
        return result;
    }
}
