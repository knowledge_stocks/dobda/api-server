package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class HelperApplyStateConverter implements AttributeConverter<HelperApplyState, Integer> {
    @Override
    public Integer convertToDatabaseColumn(HelperApplyState enumValue) {
        if (enumValue == null) {
            return null;
        }
        return enumValue.getCode();
    }

    @Override
    public HelperApplyState convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return HelperApplyState.fromCode(integer);
    }
}
