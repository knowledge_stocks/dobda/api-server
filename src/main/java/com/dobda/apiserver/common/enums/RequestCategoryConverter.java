package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RequestCategoryConverter implements AttributeConverter<RequestCategory, Integer> {
    @Override
    public Integer convertToDatabaseColumn(RequestCategory enumValue) {
        if (enumValue == null) {
            return null;
        }
        return enumValue.getCode();
    }

    @Override
    public RequestCategory convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return RequestCategory.fromCode(integer);
    }
}
