package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum HelperApplyState {
    WAITING(10),
    REJECTED(20),
    COMPLETE(30);

    private static final Map<Integer, HelperApplyState> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(HelperApplyState::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private HelperApplyState(int code)
    {
        this.code = code;
    }

    public static HelperApplyState fromCode(int code) {
        HelperApplyState result = intToEnum.get(code);
        return result;
    }
}
