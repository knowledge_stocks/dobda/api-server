package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PaymentMethodConverter implements AttributeConverter<PaymentMethod, Integer>{

	@Override
	public Integer convertToDatabaseColumn(PaymentMethod state) {
		if(state == null) {
			return null;			
		}
		return state.getCode();
	}

	@Override
	public PaymentMethod convertToEntityAttribute(Integer code) {
		if(code == null) {
			return null;
		}
		return PaymentMethod.fromCode(code);
	}
}
