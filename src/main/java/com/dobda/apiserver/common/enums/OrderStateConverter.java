package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OrderStateConverter implements AttributeConverter<OrderState, Integer>{

	@Override
	public Integer convertToDatabaseColumn(OrderState state) {
		if(state == null) {
			return null;			
		}
		return state.getCode();
	}

	@Override
	public OrderState convertToEntityAttribute(Integer code) {
		if(code == null) {
			return null;
		}
		return OrderState.fromCode(code);
	}
}
