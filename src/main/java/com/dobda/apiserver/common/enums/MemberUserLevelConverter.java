package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MemberUserLevelConverter implements AttributeConverter<MemberUserLevel, Integer> {
    @Override
    public Integer convertToDatabaseColumn(MemberUserLevel enumValue) {
        if (enumValue == null) {
            return null;
        }
        return enumValue.getCode();
    }

    @Override
    public MemberUserLevel convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return MemberUserLevel.fromCode(integer);
    }
}
