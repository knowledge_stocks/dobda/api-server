package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum RequestCategory {
    HOUSEWORK(10), // 집안일
    ENGINEERING(20), // 설치,조립,운반
    ERRAND(30), // 심부름
    CARE(40), // 요양
    HUNTING(50), // 벌레,소동물 잡기
    AGENCY(60), // 대행
    MENTORING(70), // 과외,멘토링
    COUNSEL(80), // 상담
    ETC(0); // 기타

    private static final Map<Integer, RequestCategory> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(RequestCategory::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private RequestCategory(int code)
    {
        this.code = code;
    }

    public static RequestCategory fromCode(int code) {
        RequestCategory result = intToEnum.get(code);
        return result;
    }
}
